# Cirplus Coding Challenge - Frontend

## The Task
Your task is to build a small React app that visualizes daily carbon (Co2) emisions in the world since 1958 in a meaningful way.

## What you'll need

1. The [CirPlus brand guidline](./2019-02_cirplus brand guidelines.pdf) that is in this repo. You'll find colors and typography. You don't have to follow it to the last pixel but the app should have a CirPlus look & feel.
2. The fonts in the [fonts directory](./fonts)
3. The logo is under [logo directory](./logo)
4. The API endpoint where you'll take the daily average Co2 emition data since 1958 from is [https://pkgstore.datahub.io/core/co2-ppm-daily/co2-ppm-daily_json/data/5918112417b2a061cfca919b581a3eb0/co2-ppm-daily_json.json](https://pkgstore.datahub.io/core/co2-ppm-daily/co2-ppm-daily_json/data/5918112417b2a061cfca919b581a3eb0/co2-ppm-daily_json.json) — this will return the following exemplary data structure:

    ```js
        [
            {"date": "1958-03-30", "value": "316.16"},      {"date": "1958-03-31", "value": "316.40"},
            ...
        ]
    ```

## The Process

1. Create a new repo wherever you like. Can be GitHub/GitLab/Bitbucket, doesn't matter.
1. In your new repo, create a React app
2. In your app, fetch data from this endpoint: [https://pkgstore.datahub.io/core/co2-ppm-daily/co2-ppm-daily_json/data/5918112417b2a061cfca919b581a3eb0/co2-ppm-daily_json.json](https://pkgstore.datahub.io/core/co2-ppm-daily/co2-ppm-daily_json/data/5918112417b2a061cfca919b581a3eb0/co2-ppm-daily_json.json)
3. Visualize this data in a meaningful way. We purposefully leave this very open to see what you can come up with. There's not 1 right solution, but a few example features could be:
    1. Use charts to visualize the data
    2. Filter emitions by date
    3. Use shorcut filters show last year's emitions

4. Create a README.md explaining how to test the features you have built. Feel free to add additional thoughts, e.g. why you chose certain libraries or why you implemented a feature in a certain way etc.
5. Send us an email to dev@cirplus.io when you're ready to have it reviewed

## Acceptance Criteria
* Your app fetches data from the API and displays it in the UI
* Your app uses redux to manage its state
* Your app is easy to install and run locally

## Bonus Round (not required but nice-to-have)
* Deploy the app somewhere
* Add unit tests
* Add end-to-end tests
* Dockerize the app
* Surprise us…

## Rules
There are not many rules, really. It's all about the result. However, here are a few clarifications:

* Feel free to use as many 3rd party libraries as you'd like.
* It’s ok and even encouraged to look for inspiration elsewhere but if you're taking the lazy way of just copy-and-pasting CodePen snippets: We’ll know.

## How we're evaluating the result
Prioritised from most important to least important, here's our evaluation criteria:

1. UX: Is the app working well on both desktop and mobile?
2. UI: Is the app looking great on both desktop and mobile? Did you just use bootstrap or did you go a step further to make the UI more unique?
3. Code Quality: Is the code clean, well-structured and easy to understand?
4. Performance: Is the app running smoothly? Is there any jank? How does it work with a slow internet connection?
5. The extra mile: Did you write tests? Are code quality tools such as eslint or prettier in place? Is there documentation on how to get the app running?

You do not need to hit all points, but obviously, the more the better :)
